var gulp = require("gulp");
var sass = require("gulp-sass");
var plumber = require("gulp-plumber");
var del = require("del");
var runSequence = require('run-sequence');
var htmlmin = require('gulp-htmlmin');
var uglify = require('gulp-uglify');


gulp.task("copy", function() {
    gulp.src("src/css/main.css")
        .pipe(gulp.dest("dist/css/"));
    gulp.src("src/img/*")
        .pipe(gulp.dest("dist/img/"));
    gulp.src("src/index.html")
        .pipe(gulp.dest("dist/"));
});

gulp.task("js", function() {
    gulp.src("src/js/*.js")
        .pipe(uglify())
        .pipe(gulp.dest("dist/js"));
});

gulp.task('html', function() {
    return gulp.src('src/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('dist'));
});

gulp.task('sass', function () {
    gulp.src('src/sass/main.sass')
        .pipe(plumber())
        .pipe(sass.sync())
        .pipe(gulp.dest('src/css'))
        .pipe(browserSync.stream());

});

gulp.task('clean', function() {
    del('dist/');
});

gulp.task('watch', function () {
    gulp.watch('src/sass/*/*.sass',['sass']);
    gulp.watch(['src/*.html', 'src/js/*.js'], browserSync.reload);
});

var browserSync = require("browser-sync").create();
gulp.task("server", function() {
    browserSync.init({
        server:"src"
    })
});

gulp.task("build", function() {

    runSequence("clean", "html", "js", "copy");

});

gulp.task("default",["sass", "server", "watch"]);